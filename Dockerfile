FROM python:3-alpine

MAINTAINER stozhkov.dmitry@gmayl.com

RUN pip install --upgrade pip && \
    pip cache purge

RUN adduser -D app_user

WORKDIR /home/app_user/app
RUN chown app_user:app_user /home/app_user/app

USER app_user

COPY requirements.txt /home/app_user/app/requirements.txt

ENV PATH="/home/app_user/.local/bin:${PATH}"

RUN pip install -r requirements.txt && \
    pip cache purge

EXPOSE 8000

COPY . /home/app_user/app

RUN mkdir db && \
    touch db/db.sqlite && \
    python manage.py migrate && \
    sh init.sh

CMD python manage.py runserver 0.0.0.0:8000